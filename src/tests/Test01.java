package tests;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import utils.Person;

public class Test01 {

	public static void main(String[] args) {
		List<Person> people = new ArrayList<>();
		populate(people);
		for (Person person : people) {
			// System.out.println(person);
		}

		ListIterator<Person> iterator = people.listIterator();
		/*
		 * while (iterator.hasNext()) { System.out.println(iterator.next()); }
		 */

		for (Person person : people) {
			if (person.getSurname().length() > 5) {
				// System.out.println(people.remove(person));// will throw CME
			}
		}

		/*
		 * for (int i = 0; i < people.size(); i++) { Person person = people.get(i);
		 * System.out.println(person); if (person.getSurname().length() < 6) {
		 * System.out.println("Removing " + person); people.remove(i); } }
		 */

		/*
		 * Iterator<Person> it = people.iterator(); while (it.hasNext()) { Person person
		 * = it.next(); System.out.println(person); if (person.getSurname().length() <
		 * 6) { System.out.println("Removing " + person); it.remove(); } }
		 */

		/*
		 * for (int i = people.size() - 1; i >= 0; i--) { Person person = people.get(i);
		 * System.out.println(i + " " + people.size()); System.out.println(person); if
		 * (person.getSurname().length() < 6) { people.remove(i);
		 * System.out.println("Removing: " + person); } }
		 */

		Person tocheck = new Person("Mario", "Rossi", 10);
		System.out.println(people.contains(tocheck));

		/*
		 * for (Iterator<Person> iter = people.listIterator(); iter.hasNext();) { Person
		 * person = iter.next(); System.out.println(person); if
		 * (person.getSurname().length() < 6) { System.out.println("Removing " +
		 * person); iter.remove(); } }
		 */

		/*
		 * List<Person> removedList = new ArrayList<>(); for (Person person : people) {
		 * if (person.getSurname().length() < 6) { System.out.println("Removing " +
		 * person); removedList.add(person);
		 * 
		 * } } System.out.println(people.size()); people.removeAll(removedList);
		 * System.out.println(people.size());
		 */

		/*
		 * Collections.sort(people, new PersonAgeComparator()); for (Person person :
		 * people) { System.out.println(person); }
		 */

		Vector<Person> people2 = new Vector<>();
		people2.add(new Person("Mario", "Rossi", 12));
		for (Person person : people2) {
			System.out.println(person);
		}
	}

	private static void populate(List<Person> people) {
		people.add(new Person("Mario", "Rossi", 18));
		people.add(new Person("Giuseppe", "Verdi", 17));
		people.add(new Person("Alessandro", "Del Piero", 21));
		people.add(new Person("Francesco", "Totti", 12));
		people.add(new Person("Gianluigi", "Buffon", 30));
	}

}
