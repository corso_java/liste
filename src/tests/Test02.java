package tests;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Test02 {

	public static void main(String[] args) {

		Map<Integer, String> hashmap = new HashMap<>();
		hashmap.put(3, "TreeMap");
		hashmap.put(2, "vs");
		hashmap.put(1, "HashMap");

		Map<Integer, String> treemap = new TreeMap<>();
		treemap.put(3, "TreeMap");
		treemap.put(2, "vs");
		treemap.put(1, "HashMap");

		for (Integer integer : hashmap.keySet()) {
			System.out.println(integer);
		}

		for (Integer integer : treemap.keySet()) {
			System.out.println(integer);
		}

	}

}
