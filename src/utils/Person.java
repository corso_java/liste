package utils;

public class Person {

	private Integer pk;
	private String name;
	private String surname;
	private int age;

	public Person(Integer pk, String name, String surname, int age) {
		super();
		this.pk = pk;
		this.name = name;
		this.surname = surname;
		this.age = age;
	}

	public Person(String name, String surname, int age) {
		super();
		this.name = name;
		this.surname = surname;
		this.age = age;
	}

	public Integer getPk() {
		return pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return String.format("Person [pk=%s, name=%s, surname=%s, age=%s]", pk, name, surname, age);
	}

}
