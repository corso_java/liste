package utils;

import java.util.Comparator;

public class PersonAgeComparator implements Comparator<Person> {

	@Override
	public int compare(Person o1, Person o2) {
		if (o1.getAge() != o2.getAge()) {
			return o1.getAge() - o2.getAge();
		}else {
			return o1.getSurname().length()-o2.getSurname().length();
		}
	}

}
